<%-- 
    Document   : RegistrarVenta
    Created on : 25/05/2021, 10:32:40 AM
    Author     : Christian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <div class="col-sm-4">
             <button class="btn btn-outline-info" data-toggle="modal" data-target="#ventanaModal">
                Nuevo Cliente
            </button>

            <div class="modal fade" id="ventanaModal" tabindex="-1" role="dialog" aria-labelledby="tituloVentana" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h5 id="tituloVentana">Registrar Cliente</h5>
                        <button class="close" data-dismiss="modal" aria-label="Cerrar">
                            <span aria-hidden>&times;</span>
                        </button>
                    </div>
                  <div class="modal-body">
                     <form action="">
                        <div class="form-group">
                          <label>Dni</label>
                         <input type="text" name="txtDni" class="form-control">
                        </div>
                        
                        
                        <div class="form-group">
                            <label>Nombre</label>
                         <input type="text" name="txtNombres" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Direccion</label>
                         <input type="text" name="txtTelefono" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Estado</label>
                         <input type="text" name="txtEstado" class="form-control">
                        </div>
                       
                    </form>
                  </div>
                  <div class="modal-footer">
                      <button class="btn btn-warning" type="button" data-dismiss="modal">
                        Cerrar
                      </button>
                      <button class="btn btn-success" type="button" data-dismiss="modal">
                        Agregar
                      </button>
                  </div>
                </div>
            </div>
        </div>
            
         </div>   
        <div class="d-flex">
            <div class="col-sm-5">
            <div class="card">
                <form action="Controlador" method="POST">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Datos del Cliente</label>
                        </div>
                        
                        <div class="form-group d-flex">
                            <div class="col-sm-6 d-flex">
                                <input type="text" name="codigocliente" class="form-control" placeholder="DNI">
                                <input type="submit" name="accion" value="Buscar" class="btn btn-outline-info">
                               
                            </div>
                            
                            <div class="col-sm-10">
                                <input type="text" disabled name="nombreCliente" class="form-control col-sm-6" placeholder="Datos Cliente">
                            </div>
                            
                            
                            
                            
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label>Datos del Producto</label>
                            </div>
                            
                            <div class="form-group d-flex">
                                <div class="col-sm-6 d-flex">
                                    <input type="text" name="codigoProducto" class="form-control" placeholder="Codigo">
                                    <input type="submit" name="accion" value="Buscar" class="btn btn-outline-info">
                                </div>
                               
                                <div class="col-sm-10">
                                    <input type="text" disabled name="nombreCliente" class="form-control col-sm-6" placeholder="Datos Producto">
                                </div>
    
                            </div>
                        </div>
                        <div class="form-group">
                            
                            
                            <div class="form-group d-flex">
                                <div class="col-sm-4 d-flex">
                                    <input type="text" disabled name="precio" class="form-control" placeholder="S/ 0.00">
                                   
                                </div>
                                <div class="col-sm-5">
                                    <input type="number"  name="cant" class="form-control" placeholder="Cantidad">
                                    
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" disabled name="stock" placeholder="Stock Disponible"class="form-control col-sm-6">
                                </div>
                                
                                
                                
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="accion" value="agregar" class="btn btn-info">
                        </div>
                    </div>
                </form>
            </div>
            </div>
            <div class="col-sm-7">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex ml-auto col-sm-5">
                            <label>N° Serie: </label>
                            <input type="text" disabled name="NroSerie" class="form-control">
                        </div>
                        <br>
                        <table class="table table-hover">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Nro</th>
                                    <th>Codigo</th>
                                    <th>Descripción</th>
                                    <th>Precio</th>
                                    <th>Cantidad</th>
                                    <th>SubTotal</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr> 
                            </tbody>
                        </table>
                        
                    </div>
                    <div class="card-footer d-flex">
                        <div class="col-sm-6">
                            <input type="submit" name="accion" value="GenerarVenta" class="btn btn-success">
                            <input type="submit" name="accion" value="Cancelar" class="btn btn-danger">
                        </div>
                        <div class="col-sm-4 ml-auto">
                            <input type="text" disabled name="txtTotal" class="form-control">
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
