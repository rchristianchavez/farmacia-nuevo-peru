<%-- 
    Document   : Empleado
    Created on : 25/05/2021, 10:32:05 AM
    Author     : Christian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    </head>
    <body>

        <div class="d-flex">
            <div class="card col-sm-4">
                <div class="card-body">
                    <form action="">
                        <div class="form-group">
                          <label>Dni</label>
                         <input type="text" name="txtDni" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Nombre</label>
                         <input type="text" name="txtNombres" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Telefono</label>
                         <input type="text" name="txtTelefono" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Estado</label>
                         <input type="text" name="txtEstado" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Usuario</label>
                         <input type="text" name="txUsuario" class="form-control">
                        </div>
                        <input type="submit" name="accion" value="agregar" class="btn btn-info">
                    </form>
                </div>
            </div>
            <div class="col-sm-8">
                <table class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th>DNI</th>
                            <th>NOMBRES</th>
                            <th>TELEFONO</th>
                            <th>ESTADO</th>
                            <th>USER</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <th>12345678</th>
                            <th>Ramon Quiroga Robles</th>
                            <th>987654321</th>
                            <th>Habilitado</th>
                            <th>r.quiroga</th>
                            <th><a href="#" class="btn btn-warning" >Editar</a> <span></span><a href="#" class="btn btn-danger" >Deshabilitar</a></th>
                            <tr>
                                <th>56326574</th>
                            <th>Jose Martinez Quezada</th>
                            <th>956625987</th>
                            <th>Habilitado</th>
                            <th>j.martinez</th>
                            <th><a href="#" class="btn btn-warning" >Editar</a> <span></span><a href="#" class="btn btn-danger" >Deshabilitar</a></th>
                            </tr>
                </tbody>
                </table>
            </div>
        </div>
        
    </body>
</html>
